<%-- 
    Document   : salida
    Created on : 29-03-2020, 21:47:05
    Author     : alvaro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Seccion alumno</h1>
        <%
            String nombre = (String) request.getAttribute("nombre");
            String seccion = (String) request.getAttribute("seccion");
        %>
        
        <p>
            Hola <%=nombre%> tu seccion es la: <%=seccion%>
        </p>

    </body>
</html>
